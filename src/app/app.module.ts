import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {AppComponent} from './app.component';
import {OrdersComponent} from './components/orders/orders.component';

import {HttpClientModule} from '@angular/common/http';
import {ReportsComponent} from './components/reports/reports.component';
import {ReportComponent} from './components/report/report.component';

const appRoutes: Routes = [
  {path: '', component: ReportsComponent},
  {path: '/:uuid', component: ReportComponent}
];


@NgModule({
  declarations: [
    AppComponent,
    OrdersComponent,
    ReportsComponent,
    ReportComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
