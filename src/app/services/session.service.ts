import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';

import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/catch';

import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import {Session} from '../models/session';

@Injectable()
export class SessionService {

  private baseUrl = '/openmrs/ws/rest/v1/session';

  constructor(private http: HttpClient) {
  }

  get(): Observable<Session> {
    return this.http.get(this.baseUrl)
      .map(res => res as Session);
  }
}
