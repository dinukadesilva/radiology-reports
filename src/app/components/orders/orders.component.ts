import {Component, OnInit} from '@angular/core';
import {OrderService} from '../../services/order.service';
import {Order} from '../../models/order';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css'],
  providers: [OrderService]
})
export class OrdersComponent implements OnInit {
  orders: Order[];

  constructor(private orderService: OrderService) {
  }

  ngOnInit() {
    this.orderService
      .fetch(0, 10)
      .subscribe((orders) => {
        this.orders = orders;
      });
  }

}
