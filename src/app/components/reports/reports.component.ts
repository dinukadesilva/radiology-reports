import {Component, OnInit} from '@angular/core';
import {ReportService} from '../../services/report.service';
import {Report} from '../../models/report';
import {OrderService} from '../../services/order.service';

import jsPDF from 'jspdf';
import html2pdf from 'html2pdf.js';
import $ from 'jquery';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css'],
  providers: [ReportService, OrderService]
})
export class ReportsComponent implements OnInit {
  reports: Report[];
  selectedReport: Report;

  constructor(private reportService: ReportService, private orderService: OrderService) {
  }

  setSelectReport(report) {
    this.selectedReport = report;
  }

  ngOnInit() {
    this.selectedReport = null;

    this.reportService
      .fetch(0, 10)
      .subscribe((reports) => {
        this.reports = reports;

        this.reports.map((report) => {
          this.orderService
            .get(report.radiologyOrder.uuid)
            .subscribe((order) => {
              report.radiologyOrder = order;
            });
        });
      });
  }

}
