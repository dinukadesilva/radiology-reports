import {Component, OnInit, Input, SimpleChanges, OnChanges} from '@angular/core';
import {Report} from '../../models/report';
import {OrderService} from '../../services/order.service';
import {ReportService} from '../../services/report.service';

import html2pdf from 'html2pdf.js';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent implements OnInit, OnChanges {
  @Input() uuid: string;
  report: Report;

  constructor(private reportService: ReportService, private orderService: OrderService) {
  }


  downloadReport(reportElement) {
    html2pdf(reportElement);
  }

  init() {
    this.reportService
      .get(this.uuid)
      .subscribe((report) => {
        this.report = report;

        this.orderService
          .get(report.radiologyOrder.uuid)
          .subscribe((order) => {
            report.radiologyOrder = order;
          });
      });
  }

  ngOnChanges(changes: SimpleChanges) {
    this.init();
  }

  ngOnInit() {
    this.init();
  }

}
